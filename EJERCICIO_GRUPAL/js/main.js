import api from './services/services.js';

var mask = {
	icon_url: 'https://assets.chucknorris.host/img/avatar/chuck-norris.png',
	id: 'KfY9rgE_TeeicgN0ImksvQ',
	url: '',
	value: "If you look closely, you can see Chuck Norris in the background of every scene in 'Avatar'.",
};
const searchInput = document.querySelector('#search');
const btnSearch = document.querySelector('#btn-search');
const chuckNorrisNews = document.querySelector('#chuck-norris-news');
const totalResults = document.querySelector('#totalResults');
const loader = document.querySelector('#loader');

const handlerSearch = () => {
	loader.classList.remove('d-none');
	const searching = searchInput.value;
	api.search(searching)
		.then((data) => {
			const { result, total } = data;
			totalResults.innerText = total || 0;
			// console.log(result[0].value);
			// result || []
			let markup = '';
			if (result.length > 0) {
				result.forEach((item) => {
					markup += printMarkup(item);
				});
			} else {
				markup = 'No se encontro data :c';
			}

			chuckNorrisNews.innerHTML = markup;
		})
		.catch((err) => {
			console.log('Error en la peticion SEARCH', err);
		})
		.finally(() => {
			loader.classList.add('d-none');
		});
};

const printMarkup = (item) => {
	return `
    <div class="card" style="width: 18rem">
        <img src="${item.icon_url}" class="card-img-top" alt="..." />
        <div class="card-body">
            <p class="card-text">${item.value}</p>
            <a href="#" class="btn btn-primary btn-see-more">ver mas...</a>
        </div>
	</div>`;
};

btnSearch.addEventListener('click', handlerSearch);
