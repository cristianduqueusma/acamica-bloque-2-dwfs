export default {
	search(search) {
		return new Promise((resolve, reject) => {
			fetch(`https://api.chucknorris.io/jokes/search?query=${search}`)
				.then((res) => res.json())
				.then((data) => resolve(data))
				.catch((err) => reject(err));
		});
	},
};
