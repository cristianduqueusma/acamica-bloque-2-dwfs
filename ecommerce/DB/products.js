const products = [
    {
        id: 0,
        name: "Camiseta",
        description: "Description",
        price: 1000,
        image: "https://picsum.photos/id/230/120"
    },
    {
        id: 1,
        name: "Jean",
        description: "Description",
        price: 100,
        image: "https://picsum.photos/id/231/120"
    },
    {
        id: 2,
        name: "Celular",
        description: "Description",
        price: 3000,
        image: "https://picsum.photos/id/232/120"
    },
    {
        id: 3,
        name: "Perfume",
        description: "Description",
        price: 2341,
        image: "https://picsum.photos/id/233/120"
    },
    {
        id: 4,
        name: "Mouse",
        description: "Description",
        price: 30,
        image: "https://picsum.photos/id/234/120"
    },
    {
        id: 5,
        name: "Cris",
        description: "Description",
        price: 10,
        image: "https://picsum.photos/id/235/120"
    }
];

export default products;