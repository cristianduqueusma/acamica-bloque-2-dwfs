const btnDarkMode = document.querySelector(".btn-dark-mode");
let darkMode = false;

const changeMode = () => {
  darkMode = !darkMode;
  // false---- false false
  // true ---- false true
  if(!!darkMode) {
    document.body.classList.add("dark");
  } else {
    document.body.classList.remove("dark");
  }
}

btnDarkMode.addEventListener("click", changeMode);