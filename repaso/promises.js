const API_KEY = '23QTtvAzpAVqyXKN0JBxCBMx13zFKQHD';
const ENDPOINT = 'http://api.giphy.com/v1/gifs/';
let SEARCH_LIMIT = 10;
const inputSearch = document.querySelector(".input-search");
const btnSearch = document.querySelector(".btn-dark-mode");
let searchOffset = 0; 
let gifosCount = 0;


const requestSearch = () => {
  return new Promise((resolve, reject) => {
    fetch(`${ENDPOINT}search?api_key=${API_KEY}&q=${inputSearch.value}&limit=${SEARCH_LIMIT}&offset=${searchOffset}`)
      .then((response) => response.json())
      .then((data) => resolve(data))
      .catch((error) => reject(`Error de Santi ${error}`));
  })
}

// Desde el gif 0 hasta el 9
// Desde el gif 10 hasta el 19
// Desde el gif 20 hasta el 29

const handleToSearch = () => {
  requestSearch()
  .then((response) => {
    gifosCount += SEARCH_LIMIT;
    console.log(response)
    searchOffset = searchOffset + SEARCH_LIMIT;
    //if(gifosCount === response.pagination.total_count) //Desaparecer el btn de ver mas
  })
  .catch((error) => {
    console.warn(error);
  })
}

btnSearch.addEventListener("click", handleToSearch);