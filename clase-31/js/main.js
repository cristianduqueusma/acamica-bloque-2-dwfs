/**
 * Dependences
 */
import api from './../services/services.js';

/** 
 * Variables
 */
const divContainer = document.querySelector(".container");
const inputBtn = document.querySelector(".input-pokemons");
const btnSend = document.querySelector(".btn-send");

const getPokemons = async () => {
  api.requestPokemons(inputBtn.value)
  .then((response) => {
    const { results } = response;
    let acum = 0;
    let pokemons = "";
    let pokemonsArr = [];
    let pokemonsRequest = [];
    let newPokemon;
    for (let i = 0; i < results.length; i++) {
      api.requestPokemonDetail(results[i].url)
      .then((response) => {
        acum++;
        pokemonsArr.push(response);
        inputBtn.value = "";

        if(acum === results.length) {
          pokemonsArr.sort((a, b) => {
            return (a.id > b.id) ? 1 : (b.id > a.id) ? -1 : 0;
          });

          for (let i = 0; i < pokemonsArr.length; i++) {
            pokemons += markup(pokemonsArr[i]);            
          }
          divContainer.innerHTML = pokemons;
        }
      })
    }
  })
}

/* response.sort((a, b) => {
          (a.id > b.id) ? 1 : (b.id > a.id) ? -1 : 0;
        }); */

/* const getPokemons = async () => {
  const getAllPokemon = await api.requestPokemons(inputBtn.value);
  let pokemons = "";
  let reqArray = []
  for (let i = 0; i < getAllPokemon.results.length; i++) {
    reqArray.push(api.requestPokemonDetail(getAllPokemon.results[i].url))
    const getPokemonDetail = await api.requestPokemonDetail(getAllPokemon.results[i].url);
    pokemons += markup(getPokemonDetail);
    inputBtn.value = "";
  }
  const aaaa = await Promise.all(reqArray)
  divContainer.innerHTML = pokemons;
} */

/* const requestcualqueira = async () => {
  const a = await fetch(`https://pokeapi.co/api/v2/pokemon/hola`);
  //const b = await a.json();
  console.log(a.ok)
  if (!a.ok) {
    console.log(a.status)
    handleError(a.status)
  }
} */

/* const handleError = (err) => {
  let message;
  switch (err) {
    case 200:
      message = `Un error ha ocurrido: ${err}`;
      break;
    case 404:
      message = `Un error ha ocurrido a: ${err}`
      break;
  }
  console.log(message);
} */

//async function
//requestcualqueira();

const markup = (pokemon) => {
  const { id, name, weight, sprites } = pokemon;
  return `
    <div class="pokemon">
      <p class="pokemon-order">${id}</p>
      <img src="${sprites.front_default}" alt="" class="pokemon-img">
      <p class="pokemon-name">Nombre: ${name}</p>
      <p class="pokemon-weight">Peso: ${weight}</p>
    </div>
  `;
}

btnSend.addEventListener("click", getPokemons);