export default {
  requestPokemons: function(limit = 20) {
    return new Promise((resolve, reject) => {
      fetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=0`)
      .then(response => response.json())
      .then(data => resolve(data))
      .catch(err => reject(err))
    })
  },
  requestPokemonDetail: function(url) {
    return new Promise((resolve, reject) => {
      fetch(`${url}`)
      .then(response => response.json())
      .then(data => resolve(data))
      .catch(err => reject(err))
    })
  }
}

/*requestPokemonDetail: function(id) {
    return new Promise((resolve, reject) => {
      fetch(`https://pokeapi.co/api/v2/pokemon/${id}/`)
      .then(response => response.json())
      .then(data => {
        if(data.status === 200) {
          resolve(data)
        } else {
          throw new Error(message);
        }
      })
    })
  }
} */


/* export const getPokemons = function(limit = 20) {
  return new Promise((resolve, reject) => {
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=0`)
    .then(response => response.json())
    .then(data => resolve(data))
    .catch(err => reject(err))
  })
}

export const pokemonDetail = function(id) {
  return new Promise((resolve, reject) => {
    fetch(`https://pokeapi.co/api/v2/pokemon/${id}/`)
    .then(response => response.json())
    .then(data => resolve(data))
    .catch(err => reject(err))
  })
} */