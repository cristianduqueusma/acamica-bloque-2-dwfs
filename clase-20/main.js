var name = 'Luis'; //String
//var nombre;
var married = true; //Boolean
var numberOfCars = 5; //Number
var f = NaN // Not a Number
var a = null;
var b;

//alert(`Hola ${nombre}`) // Pop up
//console.log(nombre); // Debbug
//const d = parseInt(prompt("Digite su edad"));
//const e = Math.floor(d);
const age = parseInt(prompt("Digite su edad"));

if(typeof age === "number" && !isNaN(age)) {
    if(age >= 18) {
        alert("Mayor de edad");
    } else {
        alert("Menor de edad");

        /* console.log(typeof age)
        console.log(age) */
        
    }
} else {
    alert(`Parse Incorrecto ${typeof age}`);
}


// Prompt => Ask to the user any kind of data
// parseInt => transform the data to a number
// Math => Aritmetic function
// toString => transform data to text(string)/
// split
// splice
// push
// pop


