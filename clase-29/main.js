// Servidores se componen de urls
// Un request debe tener un endpoint
// un endpoint es un path en particular configurado dentro del servidor
// www.netflix.com -> nombre del servidor
// www.netflix.com/api/v2/pokemon/ditto
    // api/v2/pokemon/ditto path/ENDPOINT

/* const a = 5;
const pokemonRequest = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            (a === 5) ? resolve({
                name: "Dito",
                order: 100,
            }) : reject("Ha ocurrido un problema");
        }, 2000)
    });
}

const getPokemonDetail = () => {
    pokemonRequest().then((response) => {
        document.body.innerHTML = response.name;
        console.log(response);
    })
    .catch((error) => {
        console.log(error);
    })
} */

const pokemonRequest = () => {
    return new Promise((resolve, reject) => {
        fetch('https://pokeapi.co/api/v2/pokemon/ditto')
        .then(response => response.json())
        .then(data => {
            document.getElementsByTagName('div')[0].innerHTML = `
                <img src=${data.sprites.front_default} />
                <p>${data.name}</p>
            `;
        });
    });
}

setTimeout(() => {
    pokemonRequest();
}, 5000)


const getPokemonDetail = () => {
    pokemonRequest().then((response) => {
        document.body.innerHTML = response.name;
        console.log(response);
    })
    .catch((error) => {
        console.log(error);
    })
}

document.getElementsByTagName("button")[0].addEventListener("click", () => {
    console.log("hola")
})



//getPokemonDetail();