//? Mi array de numeros
const numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

//? Funcion que me genera el array de 3 numeros
const getNumRandom = () => {
	const threeNumbers = [];
	for (let i = 0; i < 3; i++) {
		threeNumbers.push(numeros[Math.floor(Math.random() * 10) - 1]);
	}
	return threeNumbers;
};

//? Funcion que me trae la data del pokemon
const getPokemon = (num) => {
	return new Promise((resolve, reject) => {
		fetch(`https://pokeapi.co/api/v2/pokemon/${num}/`)
			.then((res) => res.json())
			.then((data) => resolve(data))
			.catch((err) => reject(err));
	});
};

//? Funcion que hace el race de las peticiones
const pokemon = () => {
	const numsRandom = getNumRandom();
	Promise.race([getPokemon(numsRandom[0]), getPokemon(numsRandom[1]), getPokemon(numsRandom[2])])
		.then((res) => {
			console.log(res);
		})
		.catch((err) => {
			console.warn(err);
		});
};

pokemon();
