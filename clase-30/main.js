// Promise Chaining -> Petición despues de q termine la anterior (SEE LINE 25 and 29)

// Promise all -> Recibe un arreglo con n promesas, y solo resuelve cuando TODAS son resolve, si al menos una falla, promise.all falla. (SEE LINE 65)

// Promise race -> Recibe un arreglo con n promesas y resuelve la primera en terminar. (SEE LINE 49)

const requestPokemons = () => {
  return new Promise((resolve, reject) => {
    fetch("https://pokeapi.co/api/v2/pokemon?limit=5&offset=0")
    .then(res => res.json())
    .then(data => resolve(data))
    .catch(err => reject(err));
  })
}

const requestPokemon = (url) => {
  return new Promise((resolve, reject) => {
    fetch(url)
    .then(res => res.json())
    .then(data => resolve(data))
    .catch(err => reject(err));
  })
}

/* const getPokemon = () => {
  requestPokemons()
  .then((response) => {
    const { results } = response; //Destructuring
    for (let i = 0; i < results.length; i++) {
      requestPokemon(results[i].url)
      .then(res => {
        console.log(res.moves);
        document.body.innerHTML += `
          <p>${res.name}</p>
          <img src=${res.sprites.front_default}>
        `;
      })
      .catch(err => console.warn(err));    
    }
  })
  .catch((err) => {
    console.warn(err);
  })
} */

const getPokemon = () => {
  requestPokemons()
  .then((response) => {
    let { results } = response; //Destructuring
    
    /* Promise.race(
      [
        requestPokemon(results[0].url),
        requestPokemon(results[1].url),
        requestPokemon(results[2].url),
        requestPokemon(results[3].url),
        requestPokemon(results[4].url),
      ]
    )
    .then((res) => {
      document.body.innerHTML += `
        <p>${res.name}</p>
        <img src=${res.sprites.front_default}>
      `;
    }); */

    //results[3].url = "dfsfdsgds"
    Promise.all(
      [
        requestPokemon(results[0].url),
        requestPokemon(results[1].url),
        requestPokemon(results[2].url),
        requestPokemon(results[3].url),
        requestPokemon(results[4].url),
      ]
    )
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.warn("Alguna de todas tuvo q haber fallado")
    });
  })
  .catch((err) => {
    console.warn(err);
  })
}

getPokemon();